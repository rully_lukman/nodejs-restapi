import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import multer from 'multer'

const app = express()
const upload: any = multer()
const userRouter = require('./api/users/user_router')
const tokenRouter = require('./api/token/token_router')

// use logger morgan
app.use(morgan('dev'))

// for parsing application/xwww-
app.use(bodyParser.urlencoded({
  extended: true
}));

// for parsing application/json
app.use(bodyParser.json());

// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));

// Handling CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  // bypass request method options
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});

// Calling All Router
app.use("/api/user", userRouter)
app.use("/api/token", tokenRouter)


// Handling Error/Empty Endpoint
app.use((req, res, next) => {
  const error: any = new Error("bad URI")
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    message: error.message || 'internal server error'
  })
})


module.exports = app