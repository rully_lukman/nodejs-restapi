import * as http from 'http'

const app = require('./app')
const port = process.env.APP_PORT || 3000;

const server = http.createServer(app);

server.listen(port, () => {
  console.log("Server Start at port " + port);
});