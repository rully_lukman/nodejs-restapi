import * as EmailValidator from 'email-validator';

module.exports = {
  inputValidation: (data: object, callBack: any) => {
    for (const [key, value] of Object.entries(data)) {
      if (!value) {
        return callBack(true, `${key} must not empty!`)
      }
      if (key === 'email') {
        if (!EmailValidator.validate(value)) {
          return callBack(true, `you must input proper ${key}!`)
        }
      }
    }
  }
}