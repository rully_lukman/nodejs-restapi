import { Router } from 'express'

const userController: any = require('./user_controller')
const router = Router()

router.post('/register', userController.register)
router.post('/login', userController.login)
router.get('/identity', userController.identity)
router.post('/logout', userController.logout)

module.exports = router