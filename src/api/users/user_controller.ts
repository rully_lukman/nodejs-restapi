const { register, login, logout } = require('./user_model')
const { errorValidation } = require('../../config/response')
const { inputValidation } = require('../../config/validation')
const { tokenValidation } = require('../token/token_model')

interface register {
  name: string
  last_name: string
  email: string
  password: string
}
interface login {
  email: string
  password: string
}
interface loginsosparkir {
  phone: string
}
interface tokenData {
  id: number,
  name: string,
  email: number,
  iat: number,
  exp: number
}

const userController = {
  login: (req: any, res: any) => {
    const postData: login = req.body
    inputValidation(postData, (err: any, msg: string) => {
      if (err) {
        return res.status(400).json({
          msg: msg
        })
      }
    })
    login(postData, (status: number, err: any, result: any) => {
      if (err) {
        return res.status(status).json({
          msg: err
        })
      }
      if (result) {
        return res.status(status).json({
          msg: 'success login',
          data: result
        })
      }
    })
  },
  register: (req: any, res: any) => {
    const postData: register = req.body
    // data validation
    inputValidation(postData, (err: any, msg: string) => {
      if (err) {
        return res.status(400).json({
          msg: msg
        })
      }
    })
    register(postData, (status: number, err: any, result: any) => {
      if (err) {
        return res.status(status).json({
          msg: err
        })
      }
      if (result) {
        return res.status(status).json({
          msg: 'success register',
          data: result
        })
      }
    })
  },
  identity: (req: any, res: any) => {
    // if there is no token
    if (req.headers.authorization === undefined) return res.status(401).json({ msg: `you're not authorized!` })
    // run token validation
    tokenValidation(req.headers.authorization, (err: boolean, msg: string, data: tokenData) => {
      if (err) return res.status(401).json({ msg: msg })
      delete data.iat
      delete data.exp
      return res.status(200).json(data)
    })
  },
  logout: (req: any, res: any) => {
    // if there is no token
    if (req.headers.authorization === undefined) return res.status(401).json({ msg: `you're not authorized!` })
    // run token validation
    tokenValidation(req.headers.authorization, (err: boolean, msg: string, data: tokenData) => {
      if (err) return res.status(401).json({ msg: msg })
      logout(data, (err: boolean, msg: string) => {
        if (err) return res.status(400).json({ msg: msg })
        return res.status(200).json({ msg: msg })
      })
    })
  },
}

module.exports = userController