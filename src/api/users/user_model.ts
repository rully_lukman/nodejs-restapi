import { nanoid } from 'nanoid'
import { genSaltSync, hashSync, compare } from 'bcrypt'
import jwt from 'jsonwebtoken'

interface register {
  name: string
  last_name: string
  email: string
  password: string
}
interface login {
  email: string
  password: string
}
interface loginsosparkir {
  phone: string
}
interface decoded {
  id: number
  name: string
  email: string
  iat: number
  exp: number
}

const nodemon = require('../../../nodemon')
const pool = require('../../config/database')
const moment = require('moment')
const userModel = {
  register: async (data: register, callBack: any) => {
    const user = await pool.query(`SELECT * FROM users WHERE email = ?`, [data.email])
    if (user[0].length > 0) {
      return callBack(400, 'you have already registered!')
    }
    const salt = genSaltSync(10);
    data.password = hashSync(data.password, salt);
    const insert = await pool.query(
      `INSERT INTO users (name, last_name, email, password, client_id, created_at, updated_at) VALUES (?,?,?,?,?,?,?)`,
      [
        data.name,
        data.last_name,
        data.email,
        data.password,
        nanoid(10),
        moment().format('YYYY-MM-DD HH:mm:ss'),
        moment().format('YYYY-MM-DD HH:mm:ss'),
      ],
    )
    if (insert[0].affectedRows > 0) {
      await delete data.password
      return callBack(200, null, data)
    }
  },
  login: async (data: login, callBack: any) => {
    const user = await pool.query(`SELECT id, CONCAT(name, ' ', last_name) as name, email, password FROM users WHERE email = ?`, [data.email])
    if (user[0].length === 0) {
      return callBack(400, 'your email is not registered!')
    }
    compare(data.password, user[0][0].password, async function (err, result) {
      if (err) return console.log(`error bycrpt`, err)
      if (!result) return callBack(400, 'wrong password!')
      await delete user[0][0].password
      // create access_token, refresh_token
      const userData = await {
        id: user[0][0].id,
        name: user[0][0].name,
        email: user[0][0].email,
      }
      const token = await jwt.sign(userData, nodemon.env.secret, { expiresIn: nodemon.env.tokenLife })
      const refreshToken = await jwt.sign(userData, nodemon.env.refreshTokenSecret, { expiresIn: nodemon.env.refreshTokenLife })
      const inputToken = await pool.query(`INSERT INTO user_tokens (user_id, access_token, refresh_token) VALUES (?,?,?) ON DUPLICATE KEY UPDATE user_id = VALUES(user_id), access_token = VALUES(access_token), refresh_token = VALUES(refresh_token)`, [userData.id, token, refreshToken])
      const response = {
        access_token: token,
        refresh_token: refreshToken
      }
      if (inputToken[0].affectedRows > 0) return callBack(200, null, response)
      return callBack(400, 'failed to login, please try again')
    });

  },
  // case demo sosparkir
  loginSosParkir: async (data: loginsosparkir, callBack: any) => {
    const user = await pool.query(`SELECT id, name, phone, image_url, created_at FROM mt_users WHERE phone = ?`, [data.phone])
    // console.log(user)
    if (user[0].length === 0) {
      return callBack(400, 'your phone is not registered!')
    }
    user[0][0].created_at = moment(user[0][0].created_at).format('YYYY-MM-DD HH:mm:ss')
    return callBack(200, null, user[0][0])
  },
  logout: async (data: decoded, callBack: any) => {
    const removeToken = await pool.query(`DELETE FROM user_tokens WHERE user_id = ?`, [data.id])
    if (removeToken[0].affectedRows > 0) return callBack(false, 'success logout')
    return callBack(true, 'failed to logout')
  },
}

module.exports = userModel