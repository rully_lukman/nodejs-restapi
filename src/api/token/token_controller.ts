const { refreshToken } = require('./token_model')
const tokenController = {
  refreshToken: (req: any, res: any) => {
    const postData = req.body
    refreshToken(postData, (err: boolean, msg: string, data: any) => {
      if (err) return res.status(401).json({ msg: msg })
      return res.status(200).json(data)
    })
  },
}

module.exports = tokenController