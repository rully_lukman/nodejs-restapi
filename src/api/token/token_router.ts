import { Router } from 'express'

const tokenController: any = require('./token_controller')
const router = Router()

router.post('/refresh', tokenController.refreshToken)

module.exports = router