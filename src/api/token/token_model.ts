import jwt from 'jsonwebtoken'

interface decoded {
  id: number
  name: string
  email: string
  iat: number
  exp: number
}

interface refresh_token {
  refresh_token: string
}

const nodemon = require('../../../nodemon')
const pool = require('../../config/database')
const tokenModel = {
  tokenValidation: (data: string, callBack: any) => {

    if (data.split(' ')[0] !== 'Bearer') return callBack(true, `you're not authorized!`)
    if (data.split(' ')[1] === nodemon.env.superToken) return callBack(false, null, { id: 0, name: "Super Admin", email: "nothing_todo_here@email.com", notes: 'you must not use this for customer access' })
    jwt.verify(data.split(' ')[1], nodemon.env.secret, async function (err: any, decoded: decoded) {
      // if cannot be verified
      if (err) return await callBack(true, `you're not authorized!`)

      // // check if access token is expired
      // if (new Date() > new Date(decoded.exp * 1000)) return await callBack(true, `your token is expired!`)
      const userData = await pool.query(`SELECT user_id FROM user_tokens WHERE user_id = ? AND access_token = ?`, [decoded.id, data.split(' ')[1]])
      if (userData[0].length < 1) return await callBack(true, `token not match!`)
      return await callBack(false, null, decoded)
    })
  },
  refreshToken: (data: refresh_token, callBack: any) => {
    jwt.verify(data.refresh_token, nodemon.env.refreshTokenSecret, async function (err: any, decoded: decoded) {
      if (err) return await callBack(true, `you're not authorized!`)
      await delete decoded.iat
      await delete decoded.exp
      const token = await jwt.sign(decoded, nodemon.env.secret, { expiresIn: nodemon.env.tokenLife })
      const refreshToken = await jwt.sign(decoded, nodemon.env.refreshTokenSecret, { expiresIn: nodemon.env.refreshTokenLife })
      const inputToken = await pool.query(`INSERT INTO user_tokens (user_id, access_token, refresh_token) VALUES (?,?,?) ON DUPLICATE KEY UPDATE user_id = VALUES(user_id), access_token = VALUES(access_token), refresh_token = VALUES(refresh_token)`, [decoded.id, token, refreshToken])
      const response = {
        access_token: token,
        refresh_token: refreshToken
      }
      if (inputToken[0].affectedRows > 0) return callBack(false, null, response)
      return callBack(true, 'failed to refresh token, please try again')
    })
  },
}

module.exports = tokenModel