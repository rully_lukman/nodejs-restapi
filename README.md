# nodejs-restapi

## Build Setup Yarn

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

```

## Build Setup NPM

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm dev

```

* You can get additional data on file_sharing folder (sql dump and api docs)

